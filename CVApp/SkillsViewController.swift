//
//  SkillsViewController.swift
//  CVApp
//
//  Created by Måns Svensson on 2018-11-06.
//  Copyright © 2018 Måns Svensson. All rights reserved.
//

import UIKit

class SkillsViewController: UIViewController {

    
    @IBOutlet weak var imageView: UIImageView!
    var images: [UIImage] = []
    override func viewDidLoad() {
        super.viewDidLoad()



        images = [
            UIImage(named: "1"),
            UIImage(named: "2"),
            UIImage(named: "3"),
            UIImage(named: "4"),
            UIImage(named: "5"),
            UIImage(named: "6")
            ] as! [UIImage]
        
        imageView.animationImages = images
        imageView.animationDuration = 0.8
        imageView.startAnimating()
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
