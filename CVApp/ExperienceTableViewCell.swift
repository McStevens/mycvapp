//
//  ExperienceTableViewCell.swift
//  CVApp
//
//  Created by Måns Svensson on 2018-11-05.
//  Copyright © 2018 Måns Svensson. All rights reserved.
//

import UIKit

class ExperienceTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var celltitle: UILabel!
    
    @IBOutlet weak var cellsubtitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
