//
//  CellDescriptionViewController.swift
//  CVApp
//
//  Created by Måns Svensson on 2018-11-05.
//  Copyright © 2018 Måns Svensson. All rights reserved.
//

import UIKit

class CellDescriptionViewController: UIViewController {

    @IBOutlet weak var DescriptionTitle: UILabel!
    @IBOutlet weak var DescriptionText: UILabel!
    
    var descTitle = ""
    var descText = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DescriptionTitle.text = descTitle
        DescriptionText.text = descText
        

        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
