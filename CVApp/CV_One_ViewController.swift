//
//  CV_One_ViewController.swift
//  CVApp
//
//  Created by Måns Svensson on 2018-10-29.
//  Copyright © 2018 Måns Svensson. All rights reserved.
//

import UIKit

class CV_One_ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    

    
    let celltitle = ["CEO of 'Måns Flyg och buss'","Worst RocketLeague player ","Sleeper"]
    let cellsubtitle = ["1997-2018","1997-2018","1997-2018"]
    let Titledesc = ["It appears i own a travel company (no i dont)","Peronal best is 17-game loosing streak in one play session","I've been sleeping every day all my life, im a professional"]
    
    
    let EduTitle = ["LBS Jönköping", "Jönköping University"]
    let Edusubtitle = ["2014-2017", "2018-2020"]
    let EduText = ["I went to this school", "I am currently going to this school"]
    
    let SectionTitle = ["Work Experience","Education"]
    
    var SectionTitleIndex = 0
    var myIndex = 0
    var mySectionIndex = 0
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return celltitle.count
        default:
            return EduTitle.count
        }

    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ExperienceTableViewCell
        switch indexPath.section {
        case 1:
            cell.celltitle.text? = EduTitle[indexPath.row]
            cell.cellsubtitle.text? = Edusubtitle[indexPath.row]
            return(cell)
        default:
            cell.celltitle.text? = celltitle[indexPath.row]
            cell.cellsubtitle.text? = cellsubtitle[indexPath.row]
            return(cell)
        }

        
       
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        myIndex = indexPath.row
        mySectionIndex = indexPath.section
        performSegue(withIdentifier: "cellSegue", sender: self)
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? CellDescriptionViewController{
            
            switch mySectionIndex {
            case 0:
                destination.descTitle = celltitle[myIndex]
                destination.descText = Titledesc[myIndex]
            default:
                destination.descTitle = EduTitle[myIndex]
                destination.descText = EduText[myIndex]
            }

            
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 30))
        headerView.backgroundColor = UIColor.black
        
        let headerLabel = UILabel(frame: CGRect(x: 8, y: 0, width: headerView.frame.width - 16, height: 30))
        headerLabel.text = SectionTitle[SectionTitleIndex]
        headerLabel.textColor = UIColor.white
        headerLabel.font = UIFont.boldSystemFont(ofSize: 14)
        headerLabel.textAlignment = .center
        headerView.addSubview(headerLabel)
        
        SectionTitleIndex += 1
        return headerView
    }
    	

    override func viewDidLoad() {
        super.viewDidLoad()

        
        // Do any additional setup after loading the view.
    }

    @IBAction func ExperienceButton(_ sender: UIButton) {
        performSegue(withIdentifier: "FirstSegue", sender: self)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
